/* global require, module */

const { json, send } = require( 'micro' );

const Ticket = require( '../../data/data.js' );
const connectWithDB = require( '../../data/db' );
const {
    checkRequestMethod,
    checkRequiredHeaders,
} = require( '../../helpers' );

const {
    METHODS: { POST },
    HEADERS: { CONTENT_TYPE },
    FORMATS: { APPLICATION_JSON },
} = require( '../../data/constants' );

connectWithDB();

const allowedMethods = [ POST ];
const requiredHeaders = [ {
    name  : CONTENT_TYPE,
    value : APPLICATION_JSON,
} ];

module.exports = async( req, res ) => {
    checkRequestMethod( allowedMethods, req.method )
        .then( () => {
            checkRequiredHeaders( requiredHeaders, req.headers )
                .then( async() => {
                    const body = await json( req );
                    const ticket = new Ticket( body );

                    return ticket.save()
                        .then( ( result ) => {
                            return send( res, 200,  result );
                        } )
                        .catch( ( { errors } ) => {
                            return send( res, 400, errors );
                        } );
                } )
                .catch( e => {
                    return send( res, 400, e );
                } );
        } )
        .catch( e => {
            return send( res, 400, e );
        } );
};
