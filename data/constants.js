/* global module */

const METHODS = {
    POST   : 'POST',
    GET    : 'GET',
    PUT    : 'PUT',
    DELETE : 'DELETE',
};

const HEADERS = { CONTENT_TYPE : 'content-type' };

const FORMATS = { APPLICATION_JSON : 'application/json' };

module.exports = {
    METHODS,
    HEADERS,
    FORMATS,
};
