/* global require, module */

const mongoose = require( 'mongoose' );

const Schema = mongoose.Schema;
const TicketSchema = new Schema( {
    url : {
        type     : String,
        required : [ true, 'Please provide an URL' ],
    },
    createdAt : {
        type     : Date,
        default  : Date.now(),
    },
    status : {
        type    : String,
        default : 'new',
        enum    : [ 'new', 'not_relevant', 'approved' ],
    },
    screenshotUrl : {
        type     : String,
        required : [ true, 'Please provide screenshotURL' ],
    },
    score : {
        type     : Number,
        required : [ true, 'Please provide a score' ],
    },
} );

const initCollection = () => {
    if ( mongoose.models.Ticket ) {
        return mongoose.model( 'Ticket' );
    }

    return mongoose.model( 'Ticket', TicketSchema );
};

module.exports = initCollection();
