/* global module */

const checkRequiredHeaders = ( requiredHeaders, requestHeaders ) => {
    return new Promise( ( resolve, reject ) => {
        const headerCheck = requiredHeaders.filter( header => {
            return !requestHeaders[ header.name ] || !requestHeaders[ header.name ].includes( header.value );
        } );

        if ( !headerCheck.length ) resolve();

        reject( {
            message : 'required header missing or wrong',
            headers : headerCheck.map( header => {
                return {
                    name          : header.name,
                    expectedValue : header.value,
                    receivedValue : requestHeaders[ header.name ],
                };
            } ),
        } );

    } );
};

const checkRequestMethod = ( allowedMethods, requestMethod ) => {
    return new Promise( ( resolve, reject ) => {
        if ( allowedMethods.includes( requestMethod ) ) resolve();

        reject( {
            message : 'request method not supported',
            allowedMethods,
            requestMethod,
        } );
    } );
};

module.exports = {
    checkRequiredHeaders,
    checkRequestMethod,
};
