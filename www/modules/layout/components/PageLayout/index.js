import React from 'react';
import PropTypes from 'prop-types';

// UI
import { Layout } from 'antd';
import './main.css';

const headerStyles = { backgroundColor : '#011936' };

class PageLayout extends React.Component {
    render() {
        const { children } = this.props;

        return (
            <Layout>
                <Layout.Header style={headerStyles}>
                    <div className={'headerInner'}>
                        <span className={'title'}>Hertha Ticket Bot</span>
                    </div>
                </Layout.Header>
                <Layout.Content>
                    <div className={'contentInner'}>
                        {children}
                        <style jsx>{`
                            .contentInner {
                                min-height: calc( 100vh - 256px);
                                background-color: white;
                            }
                        `}
                        </style>
                    </div>
                </Layout.Content>
                <Layout.Footer style={{
                    position  : 'fixed',
                    bottom    : 0,
                    width     : '100%',
                    textAlign : 'center',
                }}>
                    OFC Hauptstadt Support ©2018 Created by @yannickwrck & @phutschi
                </Layout.Footer>
            </Layout>
        );
    }
}

PageLayout.propTypes = { children : PropTypes.node.isRequired };

export default PageLayout;

